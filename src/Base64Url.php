<?php
namespace Beorn\Utils;

/**
 * URL-Safe Base64 Tools
 */
class Base64Url {
    /**
     * Encode data to URL-safe Base64 string.
     * @param  mixed $data Data to encode.
     * @return string A URL-safe Base64-encoded string containing the data.
     */
    public static function encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * Decode a string generated by encode() to its original data.
     * @param  string $string URL-safe Base64-encoded string generated by encode().
     * @return mixed The original data.
     */
    public static function decode($string) {
        $len = strlen($string);
        return base64_decode(str_pad(strtr($string, '-_', '+/'), $len + ($len % 4), '=', STR_PAD_RIGHT), true);
    }
}
